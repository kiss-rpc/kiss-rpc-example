package org.systemticks.kissrpc.example;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.systemticks.protobuf.rpc.data.MsgQueueElement;
import org.systemticks.protobuf.rpc.data.RpcServiceProxy;
import org.systemticks.protobuf.rpc.network.NetworkBroker;

import com.google.protobuf.InvalidProtocolBufferException;

import proto.example.rpc.RpcExampleProxy;

public class MainClient {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		
		
		try {
			NetworkBroker.INSTANCE().connect("localhost", 5678);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		RpcExampleProxy proxy = new RpcExampleProxy("proxy_0") {
			
			@Override
			public void greetingResponse(String reply) {
				System.out.println("Greeting reply: "+reply);
			}
		};
		
		proxy.greetingRequest("Cool Proxy");
		
		NetworkBroker.INSTANCE().registerProxy(proxy);
		
	}

}
