package org.systemticks.kissrpc.example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.systemticks.protobuf.rpc.network.NetworkBroker;

import proto.example.rpc.RpcExampleStub;


public class MainServer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		RpcExampleStub stub = new RpcExampleStub() {
			
			@Override
			public void greetingRequest(String greeting) {
				// TODO Auto-generated method stub
				System.out.println("Greeting Request from "+greeting);
				
				greetingResponse("Hello "+greeting+" from a cool stub");
			}
		};
		
		try {
			NetworkBroker.INSTANCE().startServer(5678);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		NetworkBroker.INSTANCE().registerStub(stub);
		
	}

}
